PROJECT_LIST_SERVICES += redis
PROJECT_NAME ?= redis

SHOW_CMD_HELP ?= @echo male $(1):
SHOW_TITLE_HELP ?= @echo "\t****** [ $(1) ] ******":
SHOW_TITLE_TARGET ?= @echo "\t****** $(1) ******":

TARGETS_HELP		+=	redis-help
PRE_TARGETS_ENV		+=	redis-env
PRE_TARGETS_UP		+=	redis-up
PRE_TARGETS_START	+=	redis-start
PRE_TARGETS_STOP	+=	redis-stop
PRE_TARGETS_DOWN	+=	redis-down
PRE_TARGETS_DUMP	+=
PRE_TARGETS_RESTORE	+=

ENV ?= dev
ENV_PATH ?= .
SERVICE_PATH ?= ..
SERVICE_REDIS_PATH ?= $(SERVICE_PATH)/redis

REDIS_COMPOSE_FILE_NAME ?= $(SERVICE_REDIS_PATH)/docker-compose
REDIS_COMPOSE_FILE ?= -f $(REDIS_COMPOSE_FILE_NAME).yml -f $(REDIS_COMPOSE_FILE_NAME)-$(MODE).$(ENV).yml

PROJECT_COMPOSE_FILE += $(REDIS_COMPOSE_FILE)

REDIS_PASS ?= example
REDIS_MAX_CLIENTS ?= 10000
REDIS_MAX_MEMORY ?= 0

redis-help:
	$(call SHOW_TITLE_HELP, REDIS)
	$(call SHOW_CMD_HELP, redis-help) справка \(Redis\)
	$(call SHOW_CMD_HELP, redis-up) Запускаем окружение UP \(Redis\)
	$(call SHOW_CMD_HELP, redis-start) Запускаем/стартуем остановленное приложение \(Redis\)
	$(call SHOW_CMD_HELP, redis-stop) Остановлеваем приложение \(Redis\)
	$(call SHOW_CMD_HELP, redis-rm) удаляем остановленное приложение \(Redis\)
	$(call SHOW_CMD_HELP, redis-cli) подключение консольным клиентом для DEV

redis-up:
	@docker-compose -p $(PROJECT_NAME) -f $(PROJECT_COMPOSE_FILE) up -d redis

redis-start:
	docker-compose -p $(PROJECT_NAME) -f $(PROJECT_COMPOSE_FILE) start redis

redis-stop:
	docker-compose -p $(PROJECT_NAME) -f $(PROJECT_COMPOSE_FILE) stop redis

redis-rm:
	@docker-compose -p $(PROJECT_NAME) -f $(PROJECT_COMPOSE_FILE) rm -s redis

redis-down: redis-stop redis-rm

redis-cli:
	docker-compose -p $(PROJECT_NAME) -f $(PROJECT_COMPOSE_FILE) exec -T redis redis-cli

redis-config:
	$(call SHOW_TITLE_TARGET, REDIS-CONFIG)
	@echo "CONFIG SET maxclients $(REDIS_MAX_CLIENTS)" | docker-compose -f $(PROJECT_COMPOSE_FILE) exec -T redis redis-cli
	@echo "CONFIG SET maxmemory $(REDIS_MAX_MEMORY)" | docker-compose -f $(PROJECT_COMPOSE_FILE) exec -T redis redis-cli
	@echo "CONFIG SET requirepass $(REDIS_PASS)" | docker-compose -f $(PROJECT_COMPOSE_FILE) exec -T redis redis-cli

redis-clean:

redis-env:
	@echo "# ---- REDIS ----" >> $(ENV_PATH)/.env
	@echo "REDIS_PASS="$(REDIS_PASS) >> $(ENV_PATH)/.env
	@echo "REDIS_MAX_CLIENTS="$(REDIS_MAX_CLIENTS) >> $(ENV_PATH)/.env
	@echo "REDIS_MAX_MEMORY="$(REDIS_MAX_MEMORY) >> $(ENV_PATH)/.env
	@echo "" >> $(ENV_PATH)/.env